const {Observable} = require('rxjs')

const stop$ = Observable.interval(2500).take(1);

const tick$ =
  Observable.interval(400)
    .takeUntil(stop$)
    .subscribe(x => console.log(x))
