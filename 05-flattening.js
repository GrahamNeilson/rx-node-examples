const {Observable} = require('rxjs')

// lots of different flattening stategies.
// think dealing with array of arrays
// more in web apps

const tick$ =
  Observable.interval(1000)
    .mergeMap(x => {
      return Observable.range(1,x)
    })
    .scan((acc, curr) => {
      return curr + acc
    })
    .subscribe(x => console.log(x))

