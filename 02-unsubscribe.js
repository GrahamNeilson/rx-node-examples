const {Observable} = require('rxjs')

// create an observable, an observable sequence
const observable = Observable.create(function (observer) {
  observer.next(1);
  observer.next(2);
  observer.next(3);
  setTimeout(() => {
    observer.next(4);
    observer.complete();
  }, 1000);

  setTimeout(() => {
    observer.next(5);
    observer.complete();
  }, 2000);
});

console.log('just before subscribe');

const subscription = observable.subscribe({
  next: x => console.log(x * 3),
  error: err => console.log('Error', err),
  complete: () => console.log('completed!')
})

console.log('just after subscribe');

setTimeout(() => {
  subscription.unsubscribe();
}, 1500)


console.log('just after unsubscribe');
