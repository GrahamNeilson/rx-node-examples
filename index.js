const {Observable} = require('rxjs')

Observable.from([1,2,3,4])
  .map(x => x * 2)
  .subscribe(x => console.log(x))
