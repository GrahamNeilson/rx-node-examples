const {Observable} = require('rxjs')

// create an observable, an observable sequence
const observable = Observable.create(function (observer) {
  // Observables are synchronous
  observer.next(1);
  observer.next(2);
  observer.next(3);
  // unless they're not
  setTimeout(() => {
    observer.next(4);
    observer.complete();
  }, 1000);
});

console.log('just before subscribe');

// you'll more likekly see things like this
// pass in 3 functions, onNext, OnError, OnCompleted, in that order.
observable.subscribe(x=> {
  console.log(x * 3)
})

console.log('just after subscribe');
