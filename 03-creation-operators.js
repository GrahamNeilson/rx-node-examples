const {Observable} = require('rxjs')

// will be examples in web apps, but

Observable.from(['a', 'b', 'c'])
  .subscribe(x => console.log(x))

Observable.range(10, 6)
  .subscribe(x => console.log(x))

Observable.zip(
  Observable.from(['a', 'b', 'c']),
  Observable.range(1, 3).map(x => x * 10)
).subscribe(x => console.log(x))

Observable.interval(500)
  .subscribe(x => console.log(x))
