const {Observable} = require('rxjs')

// create an observable, an observable sequence
const observable = Observable.create(function (observer) {
  // Observables are synchronous
  observer.next(1);
  observer.next(2);
  observer.next(3);
  // unless they're not
  setTimeout(() => {
    observer.next(4);
    observer.complete();
  }, 1000);
});

console.log('just before subscribe');

// subscribe, passing an observer.
observable.subscribe({
  next: x => console.log(x * 3),
  error: err => console.log('Error', err),
  complete: () => console.log('completed!')
})

console.log('just after subscribe');
